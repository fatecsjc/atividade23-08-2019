package br.edu.fatecsjc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainApplication {

    private static final Logger LOGGER = Logger.getLogger("JPA");

    public static void main(String[] args) {

        MainApplication mainApplication = new MainApplication();
        mainApplication.run();
    }

    private void run() {

        EntityManagerFactory factory = null;
        EntityManager entityManager = null;

        try {
            factory = Persistence.createEntityManagerFactory("PersistenceUnit");
            entityManager = factory.createEntityManager();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (entityManager != null)
                entityManager.close();
            if (factory != null)
                factory.close();
        }
    }

    private void persistPerson(EntityManager entityManager) {

        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            Usuario usuario1 = new Usuario(1L, "Robson", "aluno");
            entityManager.persist(usuario1);
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive())
                transaction.rollback();
        }
    }
}
